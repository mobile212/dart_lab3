import 'dart:ffi';

import 'package:dart_lab3/dart_lab3.dart' as dart_lab3;
import 'dart:io';

class Stack<E> {
  final _list = <E>[];

  void push(E value) => _list.add(value);

  E pop() => _list.removeLast();

  E get peek => _list.last;

  bool get isEmpty => _list.isEmpty;
  bool get isNotEmpty => _list.isNotEmpty;

  @override
  String toString() => _list.toString();
}

bool isDigit(String c) {
  return c.contains(new RegExp(r'[0-9]'));
}

void main(List<String> arguments) {
  print("input ↓↓");
  String n = stdin.readLineSync()!;
  print("Calculate completed ↓↓");
  print(cal(n));
}

double cal(String n) {
  //แปลงให้เป็น postfix เสร็จ
  //แล้วค่อยคำนวน จาก postfix
  String postfix = infixToPostFix(n)!;
  return calPostFix(postfix);
}

double calPostFix(String postfix) {
  Stack value = Stack();
  for (int i = 0; i < postfix.length; i++) {
    String c = postfix[i];
    if (isDigit(c)) {
      value.push(double.parse(c));
    } else {
      double right = value.pop();
      double left = value.pop();

      if (c == "+") {
        value.push(left + right);
      } else if (c == "-") {
        value.push(left - right);
      } else if (c == "*") {
        value.push(left * right);
      } else if (c == "/") {
        value.push(left / right);
      }
    }
  }
  return value.pop();
}

String? infixToPostFix(String n) {
  Stack operand = Stack();
  String postFix = "";

  for (int i = 0; i < n.length; i++) {
    String c = n[i];
    if (isDigit(c)) {
      postFix += c;
    } else if (c == '(') {
      operand.push('(');
    } else if (c == ')') {
      while (operand.peek != '(') {
        postFix += operand.peek;
        operand.pop();
      }
      operand.pop();
    } else {
      while (operand.isNotEmpty && precedence(c) <= precedence(operand.peek)) {
        postFix += operand.peek;
        operand.pop();
      }
      operand.push(c);
    }
  }
  while (!operand.isEmpty) {
    postFix += operand.peek;
    operand.pop();
  }
  return postFix;
}

int precedence(String c) {
  if (c == '^') {
    return 3;
  } else if (c == '/' || c == '*') {
    return 2;
  } else if (c == '+' || c == '-') {
    return 1;
  } else {
    return 0;
  }
}
