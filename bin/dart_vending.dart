import 'dart:io';

class Drink {
  String number = "";
  String name = "";
  int price = 0;
  static List<Drink> listMenu = [];
  Drink(String number, String name, int price) {
    this.number = number;
    this.name = name;
    this.price = price;
  }

  set setNumber(String number) {
    this.number = number;
  }

  set setName(String name) {
    this.name = name;
  }

  set setPrice(int price) {
    this.price = price;
  }

  String get getNumber {
    return number;
  }

  String get getName {
    return name;
  }

  int get getPrice {
    return price;
  }

  @override
  String toString() {
    // TODOl: implement toString
    return getNumber + " Menu: " + getName + "  Price: " + getPrice.toString();
  }
}

void main(List<String> arguments) {
  List<Drink> listMenu = [];
  setMenu(listMenu);
  print("Please choose your menu from below ");
  printMenu(listMenu);

  String number = stdin.readLineSync()!;
  int choice = int.parse(number);

  print("Your order is " +
      listMenu[choice - 1].getName +
      " price : " +
      listMenu[choice - 1].getPrice.toString());

  int total = 0;
  int money = 0;

  while (total < listMenu[choice - 1].getPrice) {
    print("Please input your money");
    money = int.parse(stdin.readLineSync()!);

    total = total + money;

    print("Total : $total");
  }
  print("Thank you for your purchase!");
}

void setMenu(List<Drink> listMenu) {
  Drink menu1 = Drink("1", "Latte", 55);
  listMenu.add(menu1);
  Drink menu2 = Drink("2", "Cappuchino", 55);
  listMenu.add(menu2);
  Drink menu3 = Drink("3", "Mocca", 55);
  listMenu.add(menu3);
  Drink menu4 = Drink("4", "Green Tea", 50);
  listMenu.add(menu4);
  Drink menu5 = Drink("5", "Thai Tea", 50);
  listMenu.add(menu5);
}

void printMenu(List<Drink> listMenu) {
  for (int i = 0; i < listMenu.length; i++) {
    print(listMenu[i]);
  }
}
